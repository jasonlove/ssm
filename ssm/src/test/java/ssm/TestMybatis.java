package ssm;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;

import ghw.ssm.found.entity.UserOrg;
import ghw.ssm.found.service.UserService;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring.xml","classpath:spring-mybatis.xml"})
public class TestMybatis {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TestMybatis.class);

	//private ApplicationContext ac;
	@Autowired
	private UserService userService;
	@Test
	public void test() {
//		ac=new ClassPathXmlApplicationContext(new String[]{"classpath:spring.xml","classpath:spring-mybatis.xml"});
//		userService=(UserServiceImpl) ac.getBean("userService");
		UserOrg userOrg=userService.getUserById("x10019");
		logger.info(JSON.toJSON(userOrg));
		System.out.println(userOrg.getUserName());
	}

}
