package ssm;

import org.apache.log4j.Logger;
import static org.junit.Assert.*;

import java.util.List;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omg.CORBA.PRIVATE_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion.User;

import ghw.ssm.found.service.UserService;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring.xml","classpath:spring-mybatis.xml"})
public class TestUser {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TestUser.class);

	@Autowired
	private UserService userService;
	

	@Test
	public void test1() {
		ghw.ssm.found.entity.User user=userService.getUserByPrimaryId("1001");
		
		logger.info(JSON.toJSONStringWithDateFormat(user, "yyyy-mm-dd",SerializerFeature.WriteDateUseDateFormat));
		
	}
	@Test
	public void test2() {
		List<ghw.ssm.found.entity.User> user=userService.getAll();
		
		logger.info(JSON.toJSONStringWithDateFormat(user, "yyyy-mm-dd",SerializerFeature.WriteDateUseDateFormat));
		
	}
	@Test
	public void test3() {
		List<ghw.ssm.found.entity.User> user=userService.getAll2();
		
		logger.info(JSON.toJSONStringWithDateFormat(user, "yyyy-mm-dd",SerializerFeature.WriteDateUseDateFormat));
		
	}
	@Test
	public void test4() {
		List<ghw.ssm.found.entity.User> user=userService.getAll3();
		
		logger.info(JSON.toJSONStringWithDateFormat(user, "yyyy-mm-dd",SerializerFeature.WriteDateUseDateFormat));
		
	}
	@Test
	public void test5() {
		List<ghw.ssm.found.entity.User> user=userService.getAll4();
		
		logger.info(JSON.toJSONStringWithDateFormat(user, "yyyy-mm-dd",SerializerFeature.WriteDateUseDateFormat));
		
	}

}
