package ghw.ssm.found.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ghw.ssm.found.entity.*;
import ghw.ssm.found.mapper.UserMapper;
import ghw.ssm.found.mapper.UserOrgMapper;
@Service("userService")
public class UserServiceImpl implements UserService {
	@Autowired
	private UserOrgMapper userOrgMapper;
	@Autowired
	private UserMapper userMapper;
	@Override
	public UserOrg getUserById(String id) {
		// TODO Auto-generated method stub
		return userOrgMapper.selectById(id);
	}
	@Override
	public ghw.ssm.found.entity.User getUserByPrimaryId(String id) {
		// TODO Auto-generated method stub
		
		return userMapper.selectByPrimaryKey(id);
	}
	@Override
	public List<User> getAll() {
		// TODO Auto-generated method stub
		return userMapper.getAllUsers();
	}
	@Override
	public List<User> getAll2() {
		// TODO Auto-generated method stub
		return userMapper.getAllUsers2();
	}
	@Override
	public List<User> getAll3() {
		// TODO Auto-generated method stub
		return userMapper.getAllUsers3();
	}
	@Override
	public List<User> getAll4() {
		// TODO Auto-generated method stub
		return userMapper.getAllUsers4();
	}

}
