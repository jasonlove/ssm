package ghw.ssm.found.service;

import java.util.List;

import ghw.ssm.found.entity.*;

public interface UserService {
	public UserOrg getUserById(String key);
	public User getUserByPrimaryId(String id);
	public List<User> getAll();
	public List<User> getAll2();
	public List<User> getAll3();
	public List<User> getAll4();
	
}
