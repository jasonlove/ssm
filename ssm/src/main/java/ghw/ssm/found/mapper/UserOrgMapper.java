package ghw.ssm.found.mapper;

import ghw.ssm.found.entity.UserOrg;
import ghw.ssm.found.entity.UserOrgKey;

public interface UserOrgMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_USER_ORG
     *
     * @mbggenerated Mon Feb 20 11:00:58 CST 2017
     */
    int deleteByPrimaryKey(UserOrgKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_USER_ORG
     *
     * @mbggenerated Mon Feb 20 11:00:58 CST 2017
     */
    int insert(UserOrg record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_USER_ORG
     *
     * @mbggenerated Mon Feb 20 11:00:58 CST 2017
     */
    int insertSelective(UserOrg record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_USER_ORG
     *
     * @mbggenerated Mon Feb 20 11:00:58 CST 2017
     */
    UserOrg selectByPrimaryKey(UserOrgKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_USER_ORG
     *
     * @mbggenerated Mon Feb 20 11:00:58 CST 2017
     */
    int updateByPrimaryKeySelective(UserOrg record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_USER_ORG
     *
     * @mbggenerated Mon Feb 20 11:00:58 CST 2017
     */
    int updateByPrimaryKey(UserOrg record);
    
    UserOrg selectById(String id);
}