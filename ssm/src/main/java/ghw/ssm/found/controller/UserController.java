package ghw.ssm.found.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import ghw.ssm.found.entity.UserOrg;
import ghw.ssm.found.service.UserService;
@RequestMapping("/userController")
@Controller
public class UserController {
	@Resource
	UserService userService;
	@RequestMapping("/selectById/{id}")
	public String selectById(@PathVariable String id,HttpServletRequest request){
		UserOrg userOrg=userService.getUserById(id);
		request.setAttribute("userOrg", userOrg);
		return "showUser";
		
	}
}
